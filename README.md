All dataset and software files have migrated to figshare.
The public figshare repository can be located
at DOI 10.6084/m9.figshare.7874612 or the following URL:

https://dx.doi.org/10.6084/m9.figshare.7874612

